package models

import "net/http"

type Inf struct {
	Name       string  `json:"name"`
	Percentage float32 `json:"percent"`
	Extra      string  `json:"extra"`
}

type Msg struct {
	Msg  string `json:"msg"`
	Code int    `json:"code"`
	Version
}

type Version struct {
	Version string `json:"version"`
	Name    string `json:"name"`
}

var Client = &http.Client{}
