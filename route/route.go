package route

import (
	"c02/models"
	"c02/rely"
	"c02/settings"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/mattn/go-colorable"
)

var TotalNum = 0

func SetUp() *gin.Engine {
	// 处理cmd乱码
	gin.ForceConsoleColor()
	gin.DefaultWriter = colorable.NewColorableStdout()
	// 开启服务
	r := gin.Default()
	// 日志中间件
	r.Use(rely.GinLogger(), rely.GinRecovery(true))
	r.LoadHTMLFiles("./index.html")
	r.Static("/static", "./static")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "首页",
		})
	})
	r.POST("/inf", func(c *gin.Context) {
		var inf models.Inf
		c.ShouldBindJSON(&inf)
		// fmt.Println(inf)
		fmt.Println(inf.Name, " -- ", inf.Percentage)
		if inf.Name == "person" || inf.Name == "cat" || inf.Name == "dog" {
			TotalNum++
			if TotalNum >= settings.Conf.ThresholdValue {
				TotalNum = 0
				pushInf(&inf)
			}
		}
		c.JSON(200, &models.Msg{
			Msg:  "success",
			Code: 200,
			Version: models.Version{
				Name:    settings.Conf.Name,
				Version: "1.0.0",
			},
		})

	})
	r.GET("/test", func(c *gin.Context) {

		c.JSON(http.StatusOK, &models.Msg{
			Msg:  "success",
			Code: 200,
			Version: models.Version{
				Name:    settings.Conf.Name,
				Version: "1.0.0",
			},
		})
	})
	return r
}

func pushInf(inf *models.Inf) {
	keys := settings.Conf.Certificate
	fmt.Println(keys)
	str := fmt.Sprintf("https://api2.pushdeer.com/message/push?pushkey=%s&text=%s", keys, "车门关闭了，但好像有"+inf.Name+"在车内")
	url := str
	resp, err := models.Client.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
}
