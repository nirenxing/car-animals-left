package main

import (
	"c02/rely"
	"c02/route"
	"c02/settings"
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.uber.org/zap"
)

func main() {
	var file string
	var size int
	flag.IntVar(&size, "size", 200, "文件大小")
	flag.StringVar(&file, "configFile", "config.yaml", "配置文件")
	flag.Parse()
	fmt.Println("file--:", file)

	// 初始化配置文件
	if err := settings.Init(file); err != nil {
		fmt.Printf("main: viper initiallize failed :%v\n", err.Error())
		return
	}
	// 2.初始化配置日志
	if err := rely.Init(settings.Conf.LogConfig); err != nil {
		fmt.Printf("main: viper initiallize failed :%v\n", err.Error())
		return
	}
	defer zap.L().Sync()
	// init pushFUNC

	zap.L().Debug("\n\nmain: logger initial success!")
	r := route.SetUp()
	fmt.Println("port:", fmt.Sprintf(":%d", settings.Conf.Port))
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", settings.Conf.Port),
		Handler: r,
	}

	go func() {
		// 开启一个goroutine启动服务
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1) // 创建一个接收信号的通道
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM) // 此处不会阻塞
	<-quit                                               // 阻塞在此，当接收到上述两种信号时才会往下执行
	zap.L().Info("Shutdown Server ...")
	// 创建一个5秒超时的context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		zap.L().Fatal("Server Shutdown: ", zap.Error(err))
	}

	zap.L().Info("Server exiting")
}
